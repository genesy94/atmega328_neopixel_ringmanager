//--------------------------------------------------------------------------------------------------
// Firmware per la gestione del ring Adafruit NeoPixel con comandi provenienti da seriale
// Created on:        15/10/2021
// Author:     Genesy
//--------------------------------------------------------------------------------------------------
#include <Adafruit_NeoPixel.h>

/*******************************************************************************
 * Definitions
 ******************************************************************************/
//Firmware version: major.minor.patch
#define FW_VERSION_MAJOR            2
#define FW_VERSION_MINOR            0
#define FW_VERSION_PATCH            0
 
#define NUM_OF_PIXELS               24      //numero di pixel del ring
#define RING_PIN                    6       //pin di connessione del ring
#define SERIAL_BAUDRATE             9600    //baudrate comunicazione seriale
#define SERIAL_TIMEOUT              2000    //timeout lettura da seriale
#define OPER_DELAY_VALUE            1       //delay fra un'operazione e l'altra

#define FLASHING_SPEED_SLOW_DELAY   800     //delay in modalità flashing slow
#define FLASHING_SPEED_NORM_DELAY   370     //delay in modalità flashing normal
#define FLASHING_SPEED_FAST_DELAY   120     //delay in modalità flashing fast
#define ROTATING_SPEED_SLOW_DELAY   60      //delay in modalità rotating slow
#define ROTATING_SPEED_NORM_DELAY   35      //delay in modalità rotating normal
#define ROTATING_SPEED_FAST_DELAY   15      //delay in modalità rotating fast

#define ON                          1
#define OFF                         0


enum event 
{
  evNone,
  evPush,
  evUp,
  evDown,
  evLeft,
  evRight,
  evUpLeft,
  evUpRight,
  evDownLeft,
  evDownRight,
  evRot
};

enum ringMode 
{
  modeFollowMoveOneColor,
  modeOnOneColor,
  modeOff,
  modeRotatingOneColor,
  modeFlashingOneColor,
  modeFollowMoveColorPerPixel,
  modeOnColorPerPixel,
  modeRotatingColorPerPixel,
  modeFlashingColorPerPixel
};

enum ringSpeed
{
  speedSlow,
  speedNormal,
  speedFast
};

enum flashingPixelState
{
  on,
  off
};


/*******************************************************************************
 * Variables
 ******************************************************************************/
ringMode currentMode                    = modeFollowMoveOneColor;   //modalità corrente del ring
ringSpeed currentSpeed                  = speedNormal;      //velocità corrente del ring
int   currentRotatingDelay;
int   currentFlashingDelay;
event eventFromJogger                   = evNone;           //evento di movimento ricevuto dal jogger
int   rotPixelNumber                    = 0;                //valore iniziale della rotazione
int   rotatingModeIndex                 = 3;                //valore utilizzato per gestire i 4 pixel in accensione nelle modalità rotating
flashingPixelState flashingState        = off;              //stato corrente dei pixel nelle modalità flashing
int   backgroundColor[3];                                   //ultimo colore di sfondo settato
int   currentColor[3];                                      //colore attuale in modalità one color
int   currentColorPerPixel[NUM_OF_PIXELS][3];               //colore attuale di ogni pixel, in modalità color per pixel
int   pixelOnMask[NUM_OF_PIXELS];                           //array che definisce quali pixel sono on/off in qualsiasi modalità di tipo "color per pixel"
float val                               = 0.0;              //valore incrementale per il sezionamento del PiGreco durante il calcolo del colore di flashing

//costanti
const float pi                          = 3.14;             //pi greco

//inizializzazione ring NeoPixel
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUM_OF_PIXELS, RING_PIN);


/*******************************************************************************
* Code
*******************************************************************************/
void initPixels()
{
  currentColor[0] = 0;
  currentColor[1] = 100;
  currentColor[2] = 100;
  backgroundColor[0] = 0;
  backgroundColor[1] = 0;
  backgroundColor[2] = 0;
  for (int i=0; i<NUM_OF_PIXELS; i++){
    currentColorPerPixel[i][0] = 0;
    currentColorPerPixel[i][1] = 100;
    currentColorPerPixel[i][2] = 100;   
    pixelOnMask[i] = ON; 
  }
}

 
void setup() 
{
  pixels.begin(); // This initializes the NeoPixel library.
  //inizializzazione del NeoPixelRing  
  Serial.begin(SERIAL_BAUDRATE);
  Serial.setTimeout(SERIAL_TIMEOUT);
  pixels.clear();
  pixels.setPixelColor(0, pixels.Color(100, 0, 0)); //pixel 0 illuminato di rosso per circa 3 sec
  pixels.show();
  delay(15);
  delay(3000);
  pixels.clear();
  pixels.show();
  initPixels();
}


void updateColorsAfterMove()
{
  int nextColor[NUM_OF_PIXELS][3];
  //aggiorna gli array dedicati ai colori in funzione dell'attuale mode view selezionato
    switch(eventFromJogger) {
      case evPush:
        for (int i=0; i<NUM_OF_PIXELS; i++){
          if(currentMode == modeFollowMoveOneColor){
            nextColor[i][0] = currentColor[0];
            nextColor[i][1] = currentColor[1];
            nextColor[i][2] = currentColor[2];    
          }
          else if(pixelOnMask[i] == ON){
            nextColor[i][0] = currentColorPerPixel[i][0];
            nextColor[i][1] = currentColorPerPixel[i][1];
            nextColor[i][2] = currentColorPerPixel[i][2];  
          }
          else {
            nextColor[i][0] = backgroundColor[0];
            nextColor[i][1] = backgroundColor[1];
            nextColor[i][2] = backgroundColor[2];    
          }
        }
        break;
      case evNone:
        for (int i=0; i<NUM_OF_PIXELS; i++){
          nextColor[i][0] = backgroundColor[0];
          nextColor[i][1] = backgroundColor[1];
          nextColor[i][2] = backgroundColor[2];    
        }
        break;        
      case evUp:
        for (int i=0; i<NUM_OF_PIXELS; i++){
          if (i<21 && i>2){
            nextColor[i][0] = backgroundColor[0];
            nextColor[i][1] = backgroundColor[1];
            nextColor[i][2] = backgroundColor[2];    
          }
          else{
            if(currentMode == modeFollowMoveOneColor){
              nextColor[i][0] = currentColor[0];
              nextColor[i][1] = currentColor[1];
              nextColor[i][2] = currentColor[2];  
            }
            else if(pixelOnMask[i] == ON){
              nextColor[i][0] = currentColorPerPixel[i][0];
              nextColor[i][1] = currentColorPerPixel[i][1];
              nextColor[i][2] = currentColorPerPixel[i][2];  
            }
            else {
              nextColor[i][0] = backgroundColor[0];
              nextColor[i][1] = backgroundColor[1];
              nextColor[i][2] = backgroundColor[2];    
            }            
          }
        }
        break; 
      case evRight:
        for (int i=0; i<NUM_OF_PIXELS; i++){
          if (i<3  || i>8){
            nextColor[i][0] = backgroundColor[0];
            nextColor[i][1] = backgroundColor[1];
            nextColor[i][2] = backgroundColor[2];    
          }
          else{
            if(currentMode == modeFollowMoveOneColor){
              nextColor[i][0] = currentColor[0];
              nextColor[i][1] = currentColor[1];
              nextColor[i][2] = currentColor[2];  
            }
            else if(pixelOnMask[i] == ON){
              nextColor[i][0] = currentColorPerPixel[i][0];
              nextColor[i][1] = currentColorPerPixel[i][1];
              nextColor[i][2] = currentColorPerPixel[i][2];  
            }
            else {
              nextColor[i][0] = backgroundColor[0];
              nextColor[i][1] = backgroundColor[1];
              nextColor[i][2] = backgroundColor[2];    
            }
          }
        }
        break; 
      case evDown:
        for (int i=0; i<NUM_OF_PIXELS; i++){
          if (i<9  || i>14){
            nextColor[i][0] = backgroundColor[0];
            nextColor[i][1] = backgroundColor[1];
            nextColor[i][2] = backgroundColor[2];    
          }
          else{
            if(currentMode == modeFollowMoveOneColor){
              nextColor[i][0] = currentColor[0];
              nextColor[i][1] = currentColor[1];
              nextColor[i][2] = currentColor[2];  
            }
            else if(pixelOnMask[i] == ON){
              nextColor[i][0] = currentColorPerPixel[i][0];
              nextColor[i][1] = currentColorPerPixel[i][1];
              nextColor[i][2] = currentColorPerPixel[i][2];  
            }     
            else {
              nextColor[i][0] = backgroundColor[0];
              nextColor[i][1] = backgroundColor[1];
              nextColor[i][2] = backgroundColor[2];    
            }     
          }
        } 
        break; 
      case evLeft:
        for (int i=0; i<NUM_OF_PIXELS; i++){
          if (i<15 || i>20){
            nextColor[i][0] = backgroundColor[0];
            nextColor[i][1] = backgroundColor[1];
            nextColor[i][2] = backgroundColor[2];    
          }
          else{
            if(currentMode == modeFollowMoveOneColor){
              nextColor[i][0] = currentColor[0];
              nextColor[i][1] = currentColor[1];
              nextColor[i][2] = currentColor[2];  
            }
            else if(pixelOnMask[i] == ON){
              nextColor[i][0] = currentColorPerPixel[i][0];
              nextColor[i][1] = currentColorPerPixel[i][1];
              nextColor[i][2] = currentColorPerPixel[i][2];  
            }   
            else {
              nextColor[i][0] = backgroundColor[0];
              nextColor[i][1] = backgroundColor[1];
              nextColor[i][2] = backgroundColor[2];    
            }          
          }
        }  
        break;
      case evUpLeft:
        for (int i=0; i<NUM_OF_PIXELS; i++){
          if (i<18){
            nextColor[i][0] = backgroundColor[0];
            nextColor[i][1] = backgroundColor[1];
            nextColor[i][2] = backgroundColor[2];    
          }
          else{
            if(currentMode == modeFollowMoveOneColor){
              nextColor[i][0] = currentColor[0];
              nextColor[i][1] = currentColor[1];
              nextColor[i][2] = currentColor[2];  
            }
            else if(pixelOnMask[i] == ON){
              nextColor[i][0] = currentColorPerPixel[i][0];
              nextColor[i][1] = currentColorPerPixel[i][1];
              nextColor[i][2] = currentColorPerPixel[i][2];  
            }            
            else {
              nextColor[i][0] = backgroundColor[0];
              nextColor[i][1] = backgroundColor[1];
              nextColor[i][2] = backgroundColor[2];    
            }          
          }
        }
        break; 
      case evUpRight:
        for (int i=0; i<NUM_OF_PIXELS; i++){
          if (i>5){
            nextColor[i][0] = backgroundColor[0];
            nextColor[i][1] = backgroundColor[1];
            nextColor[i][2] = backgroundColor[2];    
          }
          else{
            if(currentMode == modeFollowMoveOneColor){
              nextColor[i][0] = currentColor[0];
              nextColor[i][1] = currentColor[1];
              nextColor[i][2] = currentColor[2];  
            }
            else if(pixelOnMask[i] == ON){
              nextColor[i][0] = currentColorPerPixel[i][0];
              nextColor[i][1] = currentColorPerPixel[i][1];
              nextColor[i][2] = currentColorPerPixel[i][2];  
            }    
            else {
              nextColor[i][0] = backgroundColor[0];
              nextColor[i][1] = backgroundColor[1];
              nextColor[i][2] = backgroundColor[2];    
            }           
          }
        }
        break; 
      case evDownLeft:
        for (int i=0; i<NUM_OF_PIXELS; i++){
          if (i<12 || i>17){
            nextColor[i][0] = backgroundColor[0];
            nextColor[i][1] = backgroundColor[1];
            nextColor[i][2] = backgroundColor[2];    
          }
          else{
            if(currentMode == modeFollowMoveOneColor){
              nextColor[i][0] = currentColor[0];
              nextColor[i][1] = currentColor[1];
              nextColor[i][2] = currentColor[2];  
            }
            else if(pixelOnMask[i] == ON){
              nextColor[i][0] = currentColorPerPixel[i][0];
              nextColor[i][1] = currentColorPerPixel[i][1];
              nextColor[i][2] = currentColorPerPixel[i][2];  
            }    
            else {
              nextColor[i][0] = backgroundColor[0];
              nextColor[i][1] = backgroundColor[1];
              nextColor[i][2] = backgroundColor[2];    
            }      
          }
        } 
        break; 
      case evDownRight:
        for (int i=0; i<NUM_OF_PIXELS; i++){
          if (i<6 || i>11){
            nextColor[i][0] = backgroundColor[0];
            nextColor[i][1] = backgroundColor[1];
            nextColor[i][2] = backgroundColor[2];    
          }
          else{
            if(currentMode == modeFollowMoveOneColor){
              nextColor[i][0] = currentColor[0];
              nextColor[i][1] = currentColor[1];
              nextColor[i][2] = currentColor[2];  
            }
            else if(pixelOnMask[i] == ON){
              nextColor[i][0] = currentColorPerPixel[i][0];
              nextColor[i][1] = currentColorPerPixel[i][1];
              nextColor[i][2] = currentColorPerPixel[i][2];  
            }     
            else {
              nextColor[i][0] = backgroundColor[0];
              nextColor[i][1] = backgroundColor[1];
              nextColor[i][2] = backgroundColor[2];    
            }      
          }
        }  
        break;    
      case evRot:
        for (int i=0; i<NUM_OF_PIXELS; i++){
          if (i!=rotPixelNumber){
            nextColor[i][0] = backgroundColor[0];
            nextColor[i][1] = backgroundColor[1];
            nextColor[i][2] = backgroundColor[2];    
          }
          else{
            if(currentMode == modeFollowMoveOneColor){
              nextColor[i][0] = currentColor[0];
              nextColor[i][1] = currentColor[1];
              nextColor[i][2] = currentColor[2];  
            }
            else if(pixelOnMask[i] == ON){
              nextColor[i][0] = currentColorPerPixel[i][0];
              nextColor[i][1] = currentColorPerPixel[i][1];
              nextColor[i][2] = currentColorPerPixel[i][2];  
            }        
            else {
              nextColor[i][0] = backgroundColor[0];
              nextColor[i][1] = backgroundColor[1];
              nextColor[i][2] = backgroundColor[2];    
            }         
          }
        }
        break;     
      default:
        for (int i=0; i<NUM_OF_PIXELS; i++){
           nextColor[i][0] = backgroundColor[0];
           nextColor[i][1] = backgroundColor[1];
           nextColor[i][2] = backgroundColor[2];    
        }
        break;  
    }
    for (int i=0; i<NUM_OF_PIXELS; i++){
      pixels.setPixelColor(i, pixels.Color(nextColor[i][0], nextColor[i][1], nextColor[i][2]));
    }
}


void setAllPixelOnOneColor()
{
  for (int i=0; i<NUM_OF_PIXELS; i++){
    pixels.setPixelColor(i, pixels.Color(currentColor[0], currentColor[1], currentColor[2]));
  }
}


void setAllPixelOnColorPerPixel()
{
  for (int i=0; i<NUM_OF_PIXELS; i++){
    if(pixelOnMask[i] == ON)
      pixels.setPixelColor(i, pixels.Color(currentColorPerPixel[i][0], currentColorPerPixel[i][1], currentColorPerPixel[i][2]));
    else
      pixels.setPixelColor(i, pixels.Color(backgroundColor[0], backgroundColor[1], backgroundColor[2]));
  }
}


void setAllPixelOff()
{
  for (int i=0; i<NUM_OF_PIXELS; i++){
    pixels.setPixelColor(i, pixels.Color(backgroundColor[0], backgroundColor[1], backgroundColor[2]));
  }
}


void calcRotatingDelay()
{
  if(currentSpeed == speedSlow)
    currentRotatingDelay = ROTATING_SPEED_SLOW_DELAY;
  else if(currentSpeed == speedFast)
    currentRotatingDelay = ROTATING_SPEED_FAST_DELAY;
  else
    currentRotatingDelay = ROTATING_SPEED_NORM_DELAY;  
}


void calcFlashingDelay()
{
  if(currentSpeed == speedSlow)
    currentFlashingDelay = FLASHING_SPEED_SLOW_DELAY;
  else if(currentSpeed == speedFast)
    currentFlashingDelay = FLASHING_SPEED_FAST_DELAY;
  else
    currentFlashingDelay = FLASHING_SPEED_NORM_DELAY;  
}

  
void loop() 
{
  char command[50];
  int bytesToRead = -1;
   
  /** Lettura comando da seriale ************************************************************/
  if (Serial.available()){
    int charRead=0;
    while(((char)charRead) != '$'){
      charRead = Serial.read();
      if(charRead==-1)  { 
        delay(OPER_DELAY_VALUE);
        continue;
      }
    }
    while(bytesToRead==-1){
      bytesToRead = (int)Serial.read();
      if(bytesToRead==-1) 
        delay(OPER_DELAY_VALUE);
    }
    if((Serial.readBytes(command, bytesToRead)!=bytesToRead) || (command[bytesToRead-1] != '#'))
      return;


  /** Analisi comando ************************************************************************/
    if (command[0]=='e' && (currentMode == modeFollowMoveOneColor || currentMode == modeFollowMoveColorPerPixel)){
      //evento di movimento del jogger
      int eventRead = (int)command[1];
      if (eventRead > evRot) 
        eventRead = evNone;
      eventFromJogger = (event)eventRead;
      if(eventFromJogger == evRot) {
        rotPixelNumber = (int)command[2];
      }
      updateColorsAfterMove();
      pixels.show();
    }
    else if (command[0]=='c'){ 
      //set a color for all pixels
      //command example: $cRGB# : RGB color components
      currentColor[0]     = (int)command[1];
      currentColor[1]     = (int)command[2];
      currentColor[2]     = (int)command[3];
    }
    else if (command[0]=='m'){
      //set mode
      if((int)command[1] != currentMode) {//spegni se cambia modalità
        pixels.clear();
        pixels.show();
      }  
      currentMode  = (int)command[1];
      if(currentMode == modeRotatingOneColor || currentMode == modeRotatingColorPerPixel) {
        currentSpeed = (int)command[2];
        calcRotatingDelay();
        rotatingModeIndex = 3;
      }
      else if (currentMode == modeFlashingOneColor || currentMode == modeFlashingColorPerPixel) {
        currentSpeed = (int)command[2];
        calcFlashingDelay();
      }
    }
    else if (command[0]=='s'){
      //set speed
      currentSpeed  = (int)command[1];
      if(currentMode == modeRotatingOneColor || currentMode == modeRotatingColorPerPixel)
        calcRotatingDelay();
      else if (currentMode == modeFlashingOneColor || currentMode == modeFlashingColorPerPixel)
        calcFlashingDelay();
    }
    else if (command[0]=='p'){
      //set pixel on/off
      int pixelOnOff = (int)command[1];
      pixelOnMask[pixelOnOff] = (int)command[2];
    }
    else if (command[0]=='l'){
      //set all pixel on/off
      for(int i=0; i<NUM_OF_PIXELS; i++) {
        pixelOnMask[i] = (int)command[i+1];
      }
    }
    else if (command[0]=='x'){
      //set pixel color
      int p = (int)command[1];
      currentColorPerPixel[p][0]     = (int)command[2];
      currentColorPerPixel[p][1]     = (int)command[3];
      currentColorPerPixel[p][2]     = (int)command[4];
    }
    else if (command[0]=='v'){
      //set all even pixel color
      int j=1;
      for(int i=0; i<NUM_OF_PIXELS; i+=2) {
        currentColorPerPixel[i][0]     = (int)command[j];
        currentColorPerPixel[i][1]     = (int)command[j+1];
        currentColorPerPixel[i][2]     = (int)command[j+2];
        j=j+3;
      }
    }
    else if (command[0]=='o'){
      //set all odd pixel color
      int j=1;
      for(int i=1; i<NUM_OF_PIXELS; i+=2) {
        currentColorPerPixel[i][0]     = (int)command[j];
        currentColorPerPixel[i][1]     = (int)command[j+1];
        currentColorPerPixel[i][2]     = (int)command[j+2];
        j=j+3;
      }
    }
    else if (command[0]=='f'){
      //get firmware version
      uint8_t fwVersion[3];
      fwVersion[0] = (uint8_t) FW_VERSION_MAJOR;
      fwVersion[1] = (uint8_t) FW_VERSION_MINOR;
      fwVersion[2] = (uint8_t) FW_VERSION_PATCH;
      Serial.write(fwVersion, 3);
    }
    else {
      //in case of wrong command
      //return: "er"
    }
    delay(OPER_DELAY_VALUE);
  }
  
//=============================================================================================
  /** Gestione modalità corrente *************************************************************/
  else {
    switch(currentMode) {
      case modeOnOneColor:
        setAllPixelOnOneColor();
        pixels.show();
        delay(OPER_DELAY_VALUE);
        break;      
      case modeOnColorPerPixel:
        setAllPixelOnColorPerPixel();
        pixels.show();
        delay(OPER_DELAY_VALUE);
        break;      
      case modeOff:
        setAllPixelOff();
        pixels.show();
        delay(OPER_DELAY_VALUE);
        break; 
      case modeRotatingOneColor:
        if(rotatingModeIndex-4 >= 0)  
          pixels.setPixelColor((rotatingModeIndex-4)%NUM_OF_PIXELS, pixels.Color(backgroundColor[0], backgroundColor[1], backgroundColor[2]));
        for (int i=rotatingModeIndex-3; i<=rotatingModeIndex; i++)
          pixels.setPixelColor(i%NUM_OF_PIXELS, pixels.Color(currentColor[0], currentColor[1], currentColor[2])); 
        pixels.show();
        if(++rotatingModeIndex == NUM_OF_PIXELS+4)
          rotatingModeIndex = 4;
        delay(currentRotatingDelay);
        break; 
      case modeRotatingColorPerPixel:
        if((rotatingModeIndex-4 >= 0) && (pixelOnMask[(rotatingModeIndex-4)%NUM_OF_PIXELS] == ON))
          pixels.setPixelColor(rotatingModeIndex-4, pixels.Color(backgroundColor[0], backgroundColor[1], backgroundColor[2])); 
        for (int i=rotatingModeIndex-3; i<=rotatingModeIndex; i++) {
          if(pixelOnMask[i%NUM_OF_PIXELS] == ON)
            pixels.setPixelColor(i%NUM_OF_PIXELS, pixels.Color(currentColorPerPixel[i%NUM_OF_PIXELS][0], currentColorPerPixel[i%NUM_OF_PIXELS][1], currentColorPerPixel[i%NUM_OF_PIXELS][2])); 
          else  
            pixels.setPixelColor(i%NUM_OF_PIXELS, pixels.Color(backgroundColor[0], backgroundColor[1], backgroundColor[2])); 
        }
        pixels.show();
        if(++rotatingModeIndex == NUM_OF_PIXELS+4)
          rotatingModeIndex = 4;
        delay(currentRotatingDelay);
        break; 
      case modeFlashingOneColor:
        if(flashingState == off) {
          setAllPixelOnOneColor();
          flashingState = on;
        }
        else {
          setAllPixelOff();
          flashingState = off;
        }
        pixels.show();
        delay(currentFlashingDelay);
        break; 
      case modeFlashingColorPerPixel: 
        if(flashingState == off) {
          setAllPixelOnColorPerPixel();
          flashingState = on;
        }
        else {
          setAllPixelOff();
          flashingState = off;
        }
        pixels.show();
        delay(currentFlashingDelay);
        break; 
      default:
        break; 
    }
  }      
}
